package com.ecard.ecard.model;


import com.google.firebase.database.Exclude;

/**
 * Created by RMB on 3/3/18.
 */
public class ProfessionsList {
    private static final String TAG = "ListItem";
    @Exclude
    String userid;
    private String username,summary,status,profession;
    private String imageurl;

    public ProfessionsList() {}



    public ProfessionsList(String username, String summary, String status, String profession, String imageurl) {
        this.username = username;
        this.summary = summary;
        this.status = status;
        this.profession = profession;
        this.imageurl = imageurl;
        //this.userid = userid;
    }
    public String getUserid() {
        return userid;
    }
    public void setUserid(String id) {
        this.userid = id;
    }


    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }


    public String getSummary() {
        return summary;
    }
    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }




    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageUrl) {
        this.imageurl = imageUrl;
    }
}