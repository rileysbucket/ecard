package com.ecard.ecard.model;

import com.google.firebase.database.Exclude;

/**
 * Created by RMB on 3/3/18.
 */
public class Users {
    private static final String TAG = "UsersList";
    @Exclude
    String userid;
    private String username,summary,status,details,profession,email,skills,experience,website,moredetail,address;
    private String imageurl;
    public Users(){}
    public Users(String username, String summary, String status,
                    String details, String profession, String email, String imageurl,
                    String skills, String experience, String website, String moredetail,String address)
    {
        this.username = username;
        this.summary = summary;
        this.status = status;
        this.details = details;
        this.profession = profession;
        this.email = email;
        this.imageurl = imageurl;
        this.skills = skills;
        this.experience = experience;
        this.website = website;
        this.moredetail = moredetail;
        this.address = address;
    }
    public String getUserid() {
        return userid;
    }
    public void setUserid(String id) {
        this.userid = id;
    }


    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }


    public String getSummary() {
        return summary;
    }
    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getDetails() {
        return details;
    }
    public void setDetails(String details) {
        this.details = details;
    }


    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageUrl) {
        this.imageurl = imageUrl;
    }
//resume
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSkills() {
        return skills;
    }

    public void setSkills(String skills) {
        this.skills = skills;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getMoredetail() {
        return moredetail;
    }

    public void setMoredetail(String moredetail) {
        this.moredetail = moredetail;
    }

    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
}