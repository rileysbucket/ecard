package com.ecard.ecard.model;

/**
 * Created by RMB on 3/3/18.
 */
public class FavoritesModal {
    private static final String TAG = "ListItem";

    private String username,summary;
    private String imageurl;

    public FavoritesModal(){}

    public FavoritesModal(String username,String summary,String imageurl) {
        this.username = username;
        this.summary = summary;
        this.imageurl = imageurl;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    public String getSummary() {
        return summary;
    }
    public void setSummary(String summary) {
        this.summary = summary;
    }
    public String getImageurl() {
        return imageurl;
    }
    public void setImageurl(String imageUrl) {
        this.imageurl = imageUrl;
    }
}