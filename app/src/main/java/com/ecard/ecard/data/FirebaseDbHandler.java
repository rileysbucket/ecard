package com.ecard.ecard.data;

import android.app.Application;

import com.google.firebase.database.FirebaseDatabase;

public class FirebaseDbHandler extends Application {


    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }
}