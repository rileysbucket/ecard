package com.ecard.ecard.data;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.ecard.ecard.R;
import com.ecard.ecard.views.fragments.AllFragment;
import com.ecard.ecard.views.fragments.CategoryFragment;
import com.ecard.ecard.model.FragmentsAdapter;
import com.ecard.ecard.network.AppStatus;

import java.util.Objects;

/**
 * Created by RMB on 3/4/18.
 */

public class BaseActivity extends AppCompatActivity {

public void checkNet(){


    if (AppStatus.getInstance(getApplicationContext()).isOnline()) {

       Toast.makeText(getApplicationContext(), "WiFi/Mobile Networks Connected!", Toast.LENGTH_SHORT).show();

    } else {
       Toast.makeText(getApplicationContext(), "Ooops! No WiFi/Mobile Networks Connected!", Toast.LENGTH_SHORT).show();
    }
}
    private int[] tabIcons = {
            //R.drawable.ic_edit_black,
            R.drawable.ic_public_black_24dp,
            R.drawable.ic_group_black_24dp,
           // R.drawable.ic_collections_bookmark_black_24dp
    };
    public void getTabPager() {
        FragmentsAdapter adapter = new FragmentsAdapter(getSupportFragmentManager());
       // adapter.addFragment(new FavFragment(),"Test");
        adapter.addFragment(new AllFragment(), "All");
        adapter.addFragment(new CategoryFragment(), "Professions");
        //  adapter.addFragment(new FavFragment(), "Favorites");
        ViewPager viewPager = findViewById(R.id.viewpager);
        viewPager.setAdapter(adapter);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);
        Objects.requireNonNull(tabs.getTabAt(0)).setIcon(tabIcons[0]);
        Objects.requireNonNull(tabs.getTabAt(1)).setIcon(tabIcons[1]);
       // tabs.getTabAt(1).setIcon(tabIcons[1]);
        // tabs.getTabAt(2).setIcon(tabIcons[2]);
}
    }
