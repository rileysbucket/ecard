package com.ecard.ecard.views.adapters;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ecard.ecard.R;
import com.ecard.ecard.model.ListItem;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by RMB on 3/18/18.
 */
public class FavoritesAdapter extends RecyclerView.Adapter<FavoritesAdapter.FavoritesViewHolder> {

    private List<ListItem> listItems;
    OnItemClickListener callback;
    private Context mContext;

    public FavoritesAdapter(List<ListItem> listItems, OnItemClickListener callback) {
        this.listItems = listItems;
        this.callback = callback;
        this.mContext = mContext;
    }
    @NonNull
    @Override
    public FavoritesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false);
        return new FavoritesViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull FavoritesViewHolder holder, int position) {
        ListItem listItem = listItems.get(position);
        holder.username.setText(listItem.getUsername());
        holder.summary.setText(listItem.getSummary());
        mContext = holder.imageUrl.getContext();
        Picasso.with(mContext).load(listItem.getImageurl()).into(holder.imageUrl);
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    class FavoritesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView username;
        public TextView summary;
        public ImageView imageUrl;

        public FavoritesViewHolder(View itemView) {
            super(itemView);

            username = itemView.findViewById(R.id.list_title);
            summary = itemView.findViewById(R.id.uSummary);
            imageUrl = itemView.findViewById(R.id.list_avatar);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            callback.onItemClick(getAdapterPosition());
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int pos);
    }
}
