package com.ecard.ecard.views.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.ecard.ecard.R;
import com.ecard.ecard.model.ProfessionsList;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by RMB on 3/18/18.
 */
public class CatListAdapter extends RecyclerView.Adapter<CatListAdapter.CategoryViewHolder> {

    private List<ProfessionsList> proflistItems;
    OnItemClickListener callback;
    private Context mContext;


    public CatListAdapter(List<ProfessionsList> proflistItems, OnItemClickListener callback) {
        this.proflistItems = proflistItems;
        this.callback = callback;
        this.mContext = mContext;
    }


    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclecat_item, parent, false);
        return new CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        ProfessionsList prolistItem = proflistItems.get(position);
        holder.username.setText(prolistItem.getUsername());
        holder.profession.setText(prolistItem.getProfession());
        holder.summary.setText(prolistItem.getSummary());
        mContext = holder.imageUrl.getContext();
        Picasso.with(mContext).load(prolistItem.getImageurl()).into(holder.imageUrl);
    }

    @Override
    public int getItemCount() {
        return proflistItems.size();
    }

    class CategoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView username;
        public TextView profession;
        public TextView summary;
        public ImageView imageUrl;

        public CategoryViewHolder(View itemView) {
            super(itemView);
            username = itemView.findViewById(R.id.list_title);
            profession = itemView.findViewById(R.id.list_proff);
            summary = itemView.findViewById(R.id.uSummary);
            imageUrl = itemView.findViewById(R.id.list_avatar);


            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            callback.onItemClick(getAdapterPosition());
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int pos);
    }
}