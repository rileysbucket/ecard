package com.ecard.ecard.views.adapters;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ecard.ecard.R;
import com.ecard.ecard.model.ListItem;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by RMB on 3/18/18.
 */
public class ProffListAdapter extends RecyclerView.Adapter<ProffListAdapter.ProffViewHolder> {

    private List<ListItem> listItems;
    OnItemClickListener callback;
    private Context mContext;

    public ProffListAdapter(List<ListItem> listItems, OnItemClickListener callback) {
        this.listItems = listItems;
        this.callback = callback;
        this.mContext = mContext;
    }
    @NonNull
    @Override
    public ProffViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.proff_item_list, parent, false);
        return new ProffViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull ProffViewHolder holder, int position) {
        ListItem listItem = listItems.get(position);
       // holder.username.setText(listItem.getUsername());
        holder.profession.setText(listItem.getProfession());
       // holder.summary.setText(listItem.getSummary());
        mContext = holder.imageUrl.getContext();
        Picasso.with(mContext).load(listItem.getImageurl()).into(holder.imageUrl);
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    class ProffViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

       // public TextView username;
        public TextView profession;
        public ImageView imageUrl;

        public ProffViewHolder(View itemView) {
            super(itemView);

            profession = itemView.findViewById(R.id.list_title);
           // summary = itemView.findViewById(R.id.uSummary);
            imageUrl = itemView.findViewById(R.id.list_avatar);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            callback.onItemClick(getAdapterPosition());
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int pos);
    }
}
