package com.ecard.ecard.views.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ecard.ecard.R;
import com.ecard.ecard.data.BaseActivity;
import com.ecard.ecard.network.NetworkChangeReceiver;
import com.ecard.ecard.views.activity.animations.Bounce;
import com.ecard.ecard.views.activity.auth.LoginActivity;
import com.ecard.ecard.views.activity.profile.UserProfileActivity;
import com.ecard.ecard.views.activity.search.SearchActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ecard.ecard.network.NetworkChangeReceiver.IS_NETWORK_AVAILABLE;


public class MainActivity extends BaseActivity {
    private static final String TAG = "MainActivity";

    @BindView(R.id.loadingProgress)
    ProgressBar progressBar;

                private FirebaseAuth.AuthStateListener authListener;
                private FirebaseAuth auth;
                //private TabLayout tabs;
                boolean flag = true;
                //private  BaseActivity baseActivity = new BaseActivity();
                @Override
                    protected void onCreate(Bundle savedInstanceState) {
                        super.onCreate(savedInstanceState);
                        setContentView(R.layout.activity_main);
                    ButterKnife.bind(this);
                        getWindow().setBackgroundDrawable(null);
                    Log.e(TAG,"logging from Main Activity");
                    Toolbar toolbar = findViewById(R.id.toolbar);
                    setSupportActionBar(toolbar);
                    setupViewPager();
                    ActionBar supportActionBar = getSupportActionBar();
                   // runGPSPermissions();
                    onFirstRun();
                   checkNet();
                    //getUserLocation();
                    IntentFilter intentFilter = new IntentFilter(NetworkChangeReceiver.NETWORK_AVAILABLE_ACTION);
                    LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {
                        @Override
                        public void onReceive(Context context, Intent intent) {
                            boolean isNetworkAvailable = intent.getBooleanExtra(IS_NETWORK_AVAILABLE, false);
                            String networkStatus = isNetworkAvailable ? "connected" : "disconnected";

                            Snackbar.make(findViewById(R.id.mainActivity), "Network Status: " + networkStatus, Snackbar.LENGTH_LONG).show();
                        }
                    }, intentFilter);

                    FirebaseDatabase database = FirebaseDatabase.getInstance();
                    auth = FirebaseAuth.getInstance();
                    final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                   runAuthListener();

                    if (progressBar != null) {
                        progressBar.setVisibility(View.GONE);
                    }

                    final  FloatingActionButton visibilityFab = findViewById(R.id.fab);
                    visibilityFab.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final Animation myAnim = AnimationUtils.loadAnimation(MainActivity.this, R.anim.bounce);
                            Bounce interpolator = new Bounce(0.2, 20);
                            myAnim.setInterpolator(interpolator);
                            visibilityFab.startAnimation(myAnim);
                            if(flag){
                                Snackbar.make(v, "Hidding", Snackbar.LENGTH_LONG)
                                        .setAction("Action", null).show();
                                visibilityFab.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_hidden));
                                flag = false;


                            }else if(!flag){
                                Snackbar.make(v, "Visible", Snackbar.LENGTH_LONG)
                                        .setAction("Action", null).show();

                                visibilityFab.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_insert_emoticon_black_24dp));
                                flag = true;

                            }
                        }
                    });

                    ImageView icon = new ImageView(this); // Create an icon
                    icon.setImageDrawable(getResources().getDrawable(R.mipmap.ic_launcher));
                }

                private void setupViewPager() {
                   getTabPager();
                }

                private  void  SignOut(){

                    auth.signOut();
                }
                @Override
                public boolean onCreateOptionsMenu(Menu menu) {
                    // Inflate the menu; this adds items to the action bar if it is present.
                    getMenuInflater().inflate(R.menu.menu_main, menu);
                    return true;
                }
                @Override
                public boolean onOptionsItemSelected(MenuItem item) {
                    switch (item.getItemId()) {

                        case R.id.action_profile:
                            Intent d = new Intent(getApplicationContext(),
                                    UserProfileActivity.class);
                            startActivity(d);
                            return true;
                        case R.id.action_search:
                            Intent s = new Intent(getApplicationContext(),
                                    SearchActivity.class);
                            startActivity(s);
                            return true;
                        case R.id.action_signout:
                            SignOut();
                            return true;
                        default:
                            return super.onOptionsItemSelected(item);
                    }
                }

                public  void runAuthListener(){
                    authListener = new FirebaseAuth.AuthStateListener() {
                        @Override
                        public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                            FirebaseUser user = firebaseAuth.getCurrentUser();
                            if (user == null) {
                                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                                finish();
                            }
                        }
                    };
                }


                private  void onFirstRun(){
                    Boolean isFirstRun = getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                            .getBoolean("isFirstRun", true);
                    if (isFirstRun) {
                        //show start activity
                        startActivity(new Intent(MainActivity.this, IntroActivity.class));
                        Toast.makeText(MainActivity.this, "Welcome", Toast.LENGTH_LONG)
                                .show();

                    }
                    getSharedPreferences("PREFERENCE", MODE_PRIVATE).edit()
                            .putBoolean("isFirstRun", false).apply();
                }


                @Override
                public void onStart() {
                    super.onStart();
                    auth.addAuthStateListener(authListener);
                }
                @Override
                public void onStop() {
                    super.onStop();
                    if (authListener != null) {
                        auth.removeAuthStateListener(authListener);
                    }
                }
                @Override
                public void onPause() {
                    super.onPause();
                }
                @Override
                public void onResume() {
                    super.onResume();

                }
            }
