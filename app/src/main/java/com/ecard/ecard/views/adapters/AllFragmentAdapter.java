package com.ecard.ecard.views.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.ecard.ecard.R;
import com.ecard.ecard.model.ListItem;
import com.squareup.picasso.Picasso;
import java.util.List;

/**
 * Created by RMB on 3/18/18.
 */
public class AllFragmentAdapter extends RecyclerView.Adapter<AllFragmentAdapter.AllViewHolder> {

    private List<ListItem> listItems;
    OnItemClickListener callback;
    private Context mContext;


    public AllFragmentAdapter(List<ListItem> listItems, OnItemClickListener callback) {
        this.listItems = listItems;
        this.callback = callback;
        this.mContext = mContext;

    }

    @NonNull
    @Override
    public AllViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.all_item_tile, parent, false);
        return new AllViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AllViewHolder holder, int position) {
        ListItem listItem = listItems.get(position);
        holder.username.setText(listItem.getUsername());
        holder.profession.setText(listItem.getProfession());
        mContext = holder.imageUrl.getContext();
        Picasso.with(mContext).load(listItem.getImageurl()).into(holder.imageUrl);
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    class AllViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView username;
        public TextView profession;
        public TextView summary;
        public ImageView imageUrl;
        public TextView email;

        public AllViewHolder(View itemView) {
            super(itemView);

            username = itemView.findViewById(R.id.list_title);
            profession = itemView.findViewById(R.id.list_proff);
            summary = itemView.findViewById(R.id.uSummary);
            imageUrl = itemView.findViewById(R.id.list_avatar);
            email = itemView.findViewById(R.id.contact_email);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            callback.onItemClick(getAdapterPosition());
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int pos);
    }
}