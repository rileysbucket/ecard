package com.ecard.ecard.views.activity.profile.settings;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ecard.ecard.R;
import com.ecard.ecard.views.activity.auth.LoginActivity;
import com.ecard.ecard.views.activity.auth.RegisterActivity;
import com.ecard.ecard.views.activity.profile.UserProfileActivity;
import com.ecard.ecard.views.activity.search.SearchActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ActivitySettings extends AppCompatActivity {
    //container
    @BindView(R.id.container)
    ViewPager container;
    //Edit text
    @BindView(R.id.old_email)
    EditText oldEmail;
    @BindView(R.id.new_email)
    EditText newEmail;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.newPassword)
    EditText newPassword;


    //Funtional Buttons
    @BindView(R.id.change_password_button)
    Button changePasswordbtn;
    @BindView(R.id.save_password)
    Button savePassword;

    @BindView(R.id.sending_pass_reset_button)
    Button sendlinkbtn;
    @BindView(R.id.send_email)
    Button sendEmail;


    @BindView(R.id.change_email_button)
    Button changeEmailbtn;
    @BindView(R.id.change_email)
    Button changeEmail;

    @BindView(R.id.remove_user_button)
    Button RemoveUserbtn;
    @BindView(R.id.remove)
    FloatingActionButton removeUser;

    @BindView(R.id.sign_out)
    Button signOut;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;


    private FirebaseAuth.AuthStateListener authListener;
    //private FirebaseAuth auth;
    private FirebaseAuth firebaseAuth;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        progressBar = findViewById(R.id.progressBar);
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference mySettings = database.getReference("ecard");
        firebaseAuth = FirebaseAuth.getInstance();

        runAuthListener();



        // mAddButton = findViewById(R.id.addButton);
        // mRemoveButton = findViewById(R.id.removeButton);

//default buttons
        oldEmail.setVisibility(View.GONE);
        newEmail.setVisibility(View.GONE);
        password.setVisibility(View.GONE);
        newPassword.setVisibility(View.GONE);
        savePassword.setVisibility(View.GONE);
        sendEmail.setVisibility(View.GONE);
        changeEmail.setVisibility(View.GONE);
        removeUser.setVisibility(View.GONE);
        progressBar = findViewById(R.id.progressBar);

        if (progressBar != null) {
            progressBar.setVisibility(View.GONE);
        }
    }
    @OnClick({ R.id.change_password_button,R.id.sending_pass_reset_button,R.id.change_email_button,R.id.remove_user_button,R.id.sign_out})
    public void OnClick(View v) {
        switch (v.getId()) {

            case R.id.change_password_button:
                password.setVisibility(View.VISIBLE);
                newPassword.setVisibility(View.VISIBLE);
                savePassword.setVisibility(View.VISIBLE);
                oldEmail.setVisibility(View.GONE);
                newEmail.setVisibility(View.GONE);
                changeEmail.setVisibility(View.GONE);
                sendEmail.setVisibility(View.GONE);
                removeUser.setVisibility(View.GONE);
               //TODO:Create custom fab button/or round button
                break;

            case R.id.sending_pass_reset_button:
                oldEmail.setVisibility(View.VISIBLE);
                newEmail.setVisibility(View.GONE);
                password.setVisibility(View.GONE);
                newPassword.setVisibility(View.GONE);
                changeEmail.setVisibility(View.GONE);
                savePassword.setVisibility(View.GONE);
                sendEmail.setVisibility(View.VISIBLE);
                removeUser.setVisibility(View.GONE);
                //TODO:Create custom fab button/or round button
                break;

            case R.id.change_email_button:
                oldEmail.setVisibility(View.VISIBLE);
                newEmail.setVisibility(View.VISIBLE);
                password.setVisibility(View.GONE);
                newPassword.setVisibility(View.GONE);
                changeEmail.setVisibility(View.VISIBLE);
                savePassword.setVisibility(View.GONE);
                sendEmail.setVisibility(View.GONE);
                removeUser.setVisibility(View.GONE);
                //TODO:Create custom fab button/or round button
                break;

            case R.id.remove_user_button:
                oldEmail.setVisibility(View.GONE);
                newEmail.setVisibility(View.GONE);
                password.setVisibility(View.GONE);
                newPassword.setVisibility(View.GONE);
                changeEmail.setVisibility(View.GONE);
                savePassword.setVisibility(View.GONE);
                sendEmail.setVisibility(View.GONE);
                removeUser.setVisibility(View.VISIBLE);
                //TODO:Create custom fab button/or round button

                break;
            case R.id.sign_out:
                break;
        }
    }
    @OnClick(R.id.send_email)
    public void send_email(){
        sendEmailMethod();
    }
    public void sendEmailMethod() {
        progressBar.setVisibility(View.VISIBLE);
        if (!oldEmail.getText().toString().trim().equals("")) {
            firebaseAuth.sendPasswordResetEmail(oldEmail.getText().toString().trim())
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(ActivitySettings.this, "Reset password email is sent!", Toast.LENGTH_SHORT).show();
                                progressBar.setVisibility(View.GONE);
                            } else {
                                Toast.makeText(ActivitySettings.this, "Failed to send reset email!", Toast.LENGTH_SHORT).show();
                                progressBar.setVisibility(View.GONE);
                            }
                        }
                    });
        } else {
            oldEmail.setError("Enter email");
            progressBar.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.save_password)
    public void save_password(){
        saveUpass();
    }

    public void saveUpass(){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        progressBar.setVisibility(View.VISIBLE);
        if (user != null && !newPassword.getText().toString().trim().equals("")) {
            if (newPassword.getText().toString().trim().length() < 6) {
                newPassword.setError("Password too short, enter minimum 6 characters");
                progressBar.setVisibility(View.GONE);
            } else {
                user.updatePassword(newPassword.getText().toString())
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Toast.makeText(ActivitySettings.this, "Password is updated, sign in with new password!", Toast.LENGTH_SHORT).show();
                                    signOut();
                                    progressBar.setVisibility(View.GONE);
                                } else {
                                    Toast.makeText(ActivitySettings.this, "Failed to update password!", Toast.LENGTH_SHORT).show();
                                    progressBar.setVisibility(View.GONE);
                                }
                            }
                        });
            }
        } else if (newPassword.getText().toString().trim().equals("")) {
            newPassword.setError("Enter password");
            progressBar.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.change_email)
    public void change_email(){
        changeUserEmail();
    }

    public void changeUserEmail() {
        FirebaseUser user = firebaseAuth.getCurrentUser();
        progressBar.setVisibility(View.VISIBLE);
        if (user != null && !newEmail.getText().toString().trim().equals("")) {
            user.updateEmail(newEmail.getText().toString().trim())
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(ActivitySettings.this, "Email address is updated. Please sign in with new email id!", Toast.LENGTH_LONG).show();
                                signOut();
                                progressBar.setVisibility(View.GONE);
                            } else {
                                Toast.makeText(ActivitySettings.this, "Failed to update email!", Toast.LENGTH_LONG).show();
                                progressBar.setVisibility(View.GONE);
                            }
                        }
                    });
        } else if (newEmail.getText().toString().trim().equals("")) {
            newEmail.setError("Enter email");
            progressBar.setVisibility(View.GONE);
        }
    }


    @OnClick(R.id.remove)
    public void remove(){
        final Animation angryAnim = AnimationUtils.loadAnimation(ActivitySettings.this, R.anim.angry_btn);
        removeUser.startAnimation(angryAnim);
        removeUserMethod();
    }
    public void removeUserMethod() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        progressBar.setVisibility(View.VISIBLE);
        if (user != null) {
            user.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        Toast.makeText(ActivitySettings.this, "Your profile is deleted:( Create a account now!", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(ActivitySettings.this, RegisterActivity.class));
                        finish();
                        progressBar.setVisibility(View.GONE);
                    } else {
                        Toast.makeText(ActivitySettings.this, "Failed to delete your account!", Toast.LENGTH_SHORT).show();
                        progressBar.setVisibility(View.GONE);
                    }
                }
            });
        }
    }

    public void runAuthListener() {

        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    startActivity(new Intent(ActivitySettings.this, LoginActivity.class));
                    finish();
                }
            }
        };
    }

    //sign out method
    public void signOut() {
        firebaseAuth.signOut();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_profile:
                Intent d = new Intent(getApplicationContext(),
                        UserProfileActivity.class);
                startActivity(d);
                return true;
            case R.id.action_search:
                Intent s = new Intent(getApplicationContext(),
                        SearchActivity.class);
                startActivity(s);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(authListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (authListener != null) {
            firebaseAuth.removeAuthStateListener(authListener);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();

    }


    @OnClick(R.id.sign_out)
    public void sign_out(){
        signOut();
    }




}

