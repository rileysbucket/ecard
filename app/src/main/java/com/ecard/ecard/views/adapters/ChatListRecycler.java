package com.ecard.ecard.views.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ecard.ecard.R;
import com.ecard.ecard.views.activity.detailview.ProfileDetail;
import com.ecard.ecard.model.ListItem;
import com.squareup.picasso.Picasso;


import java.util.List;

/**
 * Created by RMB on 3/3/18.
 */

public class ChatListRecycler extends RecyclerView.Adapter<ChatListRecycler.ViewHolder> {
    private static final String TAG = "ChatListRecycler";
    // Set numbers of List in RecyclerView.
   // private static final int LENGTH = 6;
    private List<ListItem> listItems;
    private Context context;

    public ChatListRecycler(List<ListItem>listItems, Context context) {
        this.listItems = listItems;
        this.context =context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()), parent);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ListItem listItem = listItems.get(position);
        holder.username.setText(listItem.getUsername());
        holder.summary.setText(listItem.getSummary());
        Picasso.with(context).load(listItem.getImageurl()).into(holder.imageUrl);
        //holder.imageUrl.setText(listItem.getImageUrl()).into();
    }
    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView username;
        public TextView summary;
        public ImageView imageUrl;



        public ViewHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.item_tiletest, parent, false));
            //super(inflater.inflate(R.layout.item_tiletest, parent, false));
            username = itemView.findViewById(R.id.list_title);
            summary = itemView.findViewById(R.id.list_desc);
            imageUrl = itemView.findViewById(R.id.list_avatar);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = v.getContext();
                    Intent intent = new Intent(context, ProfileDetail.class);
                    intent.putExtra(ProfileDetail.EXTRA_POSITION, getAdapterPosition());
                    context.startActivity(intent);
                }
            });
        }
    }




}


