package com.ecard.ecard.views.activity.profile;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ecard.ecard.R;
import com.ecard.ecard.views.activity.MainActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CreateRes extends AppCompatActivity {
    @BindView(R.id.skills)
    EditText userSkillsEdit;

    @BindView(R.id.address)
    EditText userAddressEdit;

    @BindView(R.id.workexperience)
    EditText userExperienceEdit;

    @BindView(R.id.link1)
    EditText link1Edit;

    @BindView(R.id.email)
    EditText emailEdit;

    @BindView(R.id.moreaboutyou)
    EditText ProfileDetails;

    @BindView(R.id.userPhone)
    EditText userPhoneEdit;

    @BindView(R.id.userprofession)
    TextView tvprofession;

    //@Bind(R.id.UserProfession)
    //TextView userProfession;

   // EditText userNameEdit;
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference myRef;
    //TextView resId;
    ProgressBar mProgressBar;
    Uri imageHoldUri = null;
    //PROGRESS DIALOG
    ProgressDialog mProgress;
    //FIREBASE DATABASE FIELDS
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference resRef;
    private DatabaseReference proffRef;
    private StorageReference mStorageRef;
    String upro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_resume);
        ButterKnife.bind(this);
        firebaseAuth = FirebaseAuth.getInstance();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        resRef = mFirebaseDatabase.getReference();
        myRef = mFirebaseDatabase.getReference();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    finish();
                    Intent moveToHome = new Intent(CreateRes.this, MainActivity.class);
                    moveToHome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(moveToHome);
                }
            }
        };

        final FloatingActionButton savefab = findViewById(R.id.saveResume);
        savefab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SaveRes();
                hideKeyboard();

            }
        });
        final FloatingActionButton clearfab = findViewById(R.id.clear);
        clearfab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ClearForm();
            }
        });
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        assert user != null;
        String uid = user.getUid();
        myRef = FirebaseDatabase.getInstance().getReference().child("Users").child(uid);
        resRef = FirebaseDatabase.getInstance().getReference().child("Users").child("ProfessionalResume").child(uid);
        /* *********Work in Progress...need to retrieve one item and display it in Create Res Form as a default label)********* */
        proffRef = FirebaseDatabase.getInstance().getReference().child("Users").child("Professions").child(uid);
        //final DatabaseReference name = database.getReference("profession");
        final DatabaseReference proff = FirebaseDatabase.getInstance().getReference().child("Users").child("Resumes").child("profession");

        proffRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot uprofession) {

                upro = (String) uprofession.child("profession").getValue();
                tvprofession.setText(upro);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("error", databaseError.getMessage());
            }
        });
    }
    public void ClearForm() {
        //userNameEdit.getText().clear();
        userSkillsEdit.getText().clear();
        userAddressEdit.getText().clear();
        userExperienceEdit.getText().clear();
        emailEdit.getText().clear();
        ProfileDetails.getText().clear();
        userPhoneEdit.getText().clear();
    }
    private void SaveRes() {
//PROGRESS DIALOG
        mProgress = new ProgressDialog(this);
        mProgress.setTitle("Saving your resume");
        mProgress.setMessage("Please wait....");
        mProgress.show();
        final String userSkills, userAddress, userExp, email, website,more,phone;

        userSkills = userSkillsEdit.getText().toString().trim();
        userAddress = userAddressEdit.getText().toString().trim();
        userExp = userExperienceEdit.getText().toString().trim();
        email = emailEdit.getText().toString().trim();
        website = link1Edit.getText().toString().trim();
        more = ProfileDetails.getText().toString().trim();
        phone = userPhoneEdit.getText().toString().trim();

        if (!TextUtils.isEmpty(userSkills)) {
            resRef.child("skills").setValue(userSkills);
            resRef.child("address").setValue(userAddress);
            resRef.child("experience").setValue(userExp);
            resRef.child("email").setValue(email);
            resRef.child("website").setValue(website);
            resRef.child("moredetail").setValue(more);
            resRef.child("phone").setValue(phone);
            // myRef.child("location").setValue(userLocation);
            resRef.child("userid").setValue(firebaseAuth.getCurrentUser().getUid());

        } else {
            Toast.makeText(CreateRes.this, "Please list your skills", Toast.LENGTH_LONG).show();
            //TODO: dismiss dialog after loading data
            // need to dismiss dialog when
            mProgress.dismiss();

        }
        mProgress.dismiss();
    }

    public  void hideKeyboard(){
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }








}