package com.ecard.ecard.views.fragments;

import android.support.v4.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.ecard.ecard.R;
import com.ecard.ecard.views.adapters.CategoryAdapter;
import com.ecard.ecard.views.activity.CategoryList;
import com.ecard.ecard.model.ProfessionsList;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Provides UI for the view with Tile.
 */
public class CategoryFragment extends Fragment implements CategoryAdapter.OnItemClickListener {
    private static final String TAG = "AllFragment";


    private RecyclerView recyclerView;
    private CategoryAdapter adapter;
    // private Adapter adapter;
    private List<ProfessionsList> proffItems;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference catRef;

    private AdapterView.OnItemSelectedListener listener;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView =inflater.inflate(R.layout.recycler_view,container,false);
        proffItems = new ArrayList<>();
        adapter = new CategoryAdapter(proffItems,this);
//        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        getFirebaseData();
        //  progressBar = rootView.findViewById(R.id.progressBar);
        recyclerView = rootView.findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        int tilePadding = getResources().getDimensionPixelSize(R.dimen.tile_padding);
        recyclerView.setPadding(tilePadding, tilePadding, tilePadding, tilePadding);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        recyclerView.setAdapter(adapter);
        return recyclerView;

        //return rootView

    }

    private void getFirebaseData() {

        catRef = mFirebaseDatabase.getReference("Users").child("Professions");
        catRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                ProfessionsList data = dataSnapshot.getValue(ProfessionsList.class);
                proffItems.add(data);
                adapter.notifyDataSetChanged();

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {



            }
        });
    }

    @Override
    public void onItemClick(int pos) {

       Intent catDetail = new Intent(this.getContext(), CategoryList.class);

    ProfessionsList listItem = proffItems.get(pos);
     catDetail.putExtra(CategoryList.CAT_POSITION, listItem.getUserid());
        startActivity(catDetail);
       // AlertDialog.Builder catDetail = new AlertDialog.Builder(getActivity());


     //AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());

      /* Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.category_users);
        dialog.setTitle("Your title here");

        //ListView lv = dialog.findViewById(R.id.lv);
        dialog.setCancelable(true);
        dialog.setTitle("ListView");
        dialog.show();*/




       /* AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        alert.setContentView(R.id.category_user);
        alert.setTitle("Your title here");
        alert.setMessage(listItems.get(pos).toString());
        alert.setPositiveButton(R.string.OK,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                        dialog.dismiss();

                    }
                });

        alert.show();*/










    }
}