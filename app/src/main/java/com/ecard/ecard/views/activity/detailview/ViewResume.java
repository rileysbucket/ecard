package com.ecard.ecard.views.activity.detailview;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.ecard.ecard.R;
import com.ecard.ecard.model.ListItem;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewResume extends AppCompatActivity {
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference resumeRef;
    private List<ListItem> listItems;
    private ProgressDialog progressDialog;
    //Resume
    @BindView(R.id.user_fullname)
    TextView tvfullName;
    //@Bind(R.id.userPName) TextView txtuserProfileName;
    @BindView(R.id.user_skills)
    TextView tvuserSkills;
    @BindView(R.id.user_experience)
    TextView tvExperience;
    @BindView(R.id.contact_email)
    TextView tvcontactEmail;
    @BindView(R.id.postal_address)
    TextView tvpostalAddress;
    @BindView(R.id.user_website)
    TextView tvuserWebsite;
    @BindView(R.id.moreaboutyou)
    TextView tvuserMore;
    @BindView(R.id.userprofession)TextView tvProfession;
    @BindView(R.id.userProfileName) TextView tvName;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewresume);
        ButterKnife.bind(this);
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        resumeRef = mFirebaseDatabase.getReference("Users").child("ProfessionalResume");
        resumeRef.keepSynced(true);
        listItems = new ArrayList<>();
        ButterKnife.bind(this);
        progressDialog = new ProgressDialog(this);
    }
//TODO: delete this class,its useless
//This class is practically useless,the view has already been included in the pop up and this class should be removed


}
