    package com.ecard.ecard.views.activity.auth;

    import android.content.Context;
    import android.content.Intent;
    import android.os.Bundle;
    import android.support.annotation.NonNull;
    import android.text.TextUtils;
    import android.view.View;
    import android.view.inputmethod.InputMethodManager;
    import android.widget.Button;
    import android.widget.EditText;
    import android.widget.ProgressBar;
    import android.widget.Toast;

    import com.ecard.ecard.R;
    import com.ecard.ecard.data.BaseActivity;
    import com.ecard.ecard.views.activity.MainActivity;
    import com.google.android.gms.tasks.OnCompleteListener;
    import com.google.android.gms.tasks.Task;
    import com.google.firebase.auth.AuthResult;
    import com.google.firebase.auth.FirebaseAuth;
    import com.google.firebase.auth.FirebaseUser;
    import com.google.firebase.database.DatabaseReference;
    import com.google.firebase.database.FirebaseDatabase;

    import butterknife.BindView;
    import butterknife.ButterKnife;
    import butterknife.OnClick;

    public class LoginActivity extends BaseActivity {
        private static final String TAG = "LoginActivity";
        @BindView(R.id.email)
        EditText inputEmail;
        @BindView(R.id.password)
        EditText inputPassword;
        @BindView(R.id.authprogress)
        ProgressBar progressBar;
        // btn
        @BindView(R.id.btn_signup) Button btnSignup;
        @BindView(R.id.btn_reset_password) Button btnReset;
        @BindView(R.id.btn_login) Button btnLogin;
        private FirebaseAuth firebaseAuth;
        FirebaseAuth.AuthStateListener mAuthListener;
        String email;
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_login);
            ButterKnife.bind(this);
            checkNet();
            firebaseAuth = FirebaseAuth.getInstance();
            mAuthListener = new FirebaseAuth.AuthStateListener(){
                @Override
                public  void  onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth){
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user!=null){
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
}
                }
            };
            sessionDog();
            FirebaseUser user = firebaseAuth.getCurrentUser();
        }
        @OnClick(R.id.btn_signup)
        public void btn_signup(){
            goToRegister();
        }
        @OnClick(R.id.btn_reset_password)
        public void btn_reset_password(){
            goToReset();
        }
        @OnClick(R.id.btn_login)
        public void btn_login(){
            UserLogin();
            hideKeyboard();
        }





        private  void UserLogin(){
                    email = inputEmail.getText().toString();
                    final String password = inputPassword.getText().toString().trim();
                    final String email = inputEmail.getText().toString().trim();
                    if (TextUtils.isEmpty(email)) {

                        Toast.makeText(getApplicationContext(), "Enter email address!", Toast.LENGTH_SHORT).show();
                        inputPassword.setError("Enter email address");
                        return;
                    }
                    if (TextUtils.isEmpty(password)) {
                        Toast.makeText(getApplicationContext(), "Enter password!", Toast.LENGTH_SHORT).show();
                        inputEmail.setError("Enter password!");

                        return;
                    }
                    progressBar.setVisibility(View.VISIBLE);
                    firebaseAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            progressBar.setVisibility(View.GONE);
                            if (!task.isSuccessful()) {
                                if (password.length() < 6) {
                                    inputPassword.setError(getString(R.string.error_invalid_password));
                                } else {
                                    Toast.makeText(LoginActivity.this, getString(R.string.login_error), Toast.LENGTH_LONG).show();
                                }
                            } else {
                                String userid = firebaseAuth.getCurrentUser().getUid();
                                DatabaseReference current_user_db = FirebaseDatabase.getInstance().getReference("Users").child(userid);
                                current_user_db.setValue(true);
                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                startActivity(intent);
                                finish();

                            }
                            if
                                        (!task.isSuccessful()){

                                Toast.makeText(getApplicationContext(), "Login Failed!..Please check your network and try again", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }

        public  void sessionDog(){
            if (firebaseAuth.getCurrentUser() != null) {
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                finish();
            }
        }
        private void goToRegister(){
            startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
        }
        private void goToReset(){
            startActivity(new Intent(LoginActivity.this, ResetPasswordActivity.class));
        }
        @Override
        protected void onStart() {
            super.onStart();
            firebaseAuth.addAuthStateListener(mAuthListener);
        }
        @Override
        protected void onResume() {
            super.onResume();
            firebaseAuth.addAuthStateListener(mAuthListener);
            }
        @Override
        protected void onStop() {
            super.onStop();
            firebaseAuth.removeAuthStateListener(mAuthListener);
        }
        private void hideKeyboard() {
            View view = this.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                assert imm != null;
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }