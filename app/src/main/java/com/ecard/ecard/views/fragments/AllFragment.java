package com.ecard.ecard.views.fragments;

import android.support.v4.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import com.ecard.ecard.R;
import com.ecard.ecard.model.ListItem;
import com.ecard.ecard.views.activity.detailview.ProfileDetail;
import com.ecard.ecard.views.adapters.AllFragmentAdapter;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Provides UI for the view with Tile.
 */
public class AllFragment extends Fragment implements AllFragmentAdapter.OnItemClickListener {
    private static final String TAG = "AllFragment";


    private RecyclerView recyclerView;
    private AllFragmentAdapter adapter;
    // private Adapter adapter;
    private List<ListItem> listItems;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference myRef;

    private AdapterView.OnItemSelectedListener listener;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView =inflater.inflate(R.layout.recycler_view,container,false);
        listItems = new ArrayList<>();
        adapter = new AllFragmentAdapter(listItems,this);
//        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        getFirebaseData();
        //  progressBar = rootView.findViewById(R.id.progressBar);
        recyclerView = rootView.findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        int tilePadding = getResources().getDimensionPixelSize(R.dimen.tile_padding);
        recyclerView.setPadding(tilePadding, tilePadding, tilePadding, tilePadding);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        recyclerView.setAdapter(adapter);
        return recyclerView;

        //return rootView

    }

    private void getFirebaseData() {

        myRef = mFirebaseDatabase.getReference("Users").child("UserProfile");
        myRef.keepSynced(true);
        myRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, String s) {
                ListItem data = dataSnapshot.getValue(ListItem.class);
                listItems.add(data);
                adapter.notifyDataSetChanged();

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {


            }
        });
    }

    @Override
    public void onItemClick(int pos) {

        Intent pDetail = new Intent(this.getContext(), ProfileDetail.class);
        ListItem listItem = listItems.get(pos);
        pDetail.putExtra(ProfileDetail.EXTRA_POSITION, listItem.getUserid());
        startActivity(pDetail);
    }
    public void onStart(){
        super.onStart();


    }
    @Override
    public void onStop() {
        super.onStop();

    }
}