package com.ecard.ecard.views.activity.profile;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ecard.ecard.R;
import com.ecard.ecard.views.activity.animations.Bounce;
import com.ecard.ecard.views.activity.MainActivity;
import com.ecard.ecard.views.activity.profile.settings.ActivitySettings;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import it.sephiroth.android.library.tooltip.Tooltip;


public class UserProfileActivity extends AppCompatActivity {
    private static final String TAG = UserProfileActivity.class.getSimpleName();
    private static final int REQUEST_CAMERA = 1;
    private static final int SELECT_IMAGE = 2;
    private static final int SELECT_FILE = 3;
    @BindView(R.id.proffid) TextView profid;
    @BindView(R.id.userProfileName) EditText userNameEdit;
    @BindView(R.id.ProfileSummary) EditText userSummaryEdit;
    @BindView(R.id.UserProfession)
    AutoCompleteTextView userProfessionEdit;
    @BindView(R.id.userStatus) EditText userStatusEdit;
    @BindView(R.id.contactEmail) EditText userContactEmailEdit;
    @BindView(R.id.userAvatar) ImageView userImageavatar;
    @BindView(R.id.thumb) FloatingActionButton fileThumbNail;
    //@InjectView(R.id.thumb) ImageView userImageProfileView;
    @BindView(R.id.createProfile) Button createProfileBtn;
   // @Bind(R.id.saveResume) Button saveResumeBtn; //(COMING SOON)
   @BindView(R.id.createResume) FloatingActionButton createRes;
    //@InjectView(R.id.userLocation) TextView userLocationEdit;
    @BindView(R.id.editSettings) FloatingActionButton editSettingsbtn;
    @BindView(R.id.catspinner) Spinner profSpinner;

//update btn
@BindView(R.id.updateProf) Button saveUpdate;

    ProgressBar fileuploadProgress;
    //Firebase auth fields
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    //Firebase fields
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference myRef;
    private DatabaseReference proffRef;
    private DatabaseReference proffRefview;
    private DatabaseReference proffRefspin;
    private DatabaseReference profrefname,updateRef,updateRefprof,emptyCv;
    private DatabaseReference cvRef;
    private  StorageReference mStorageRef;
    //Image holder Uri
    Uri imageHoldUri = null;
    Uri fileHoldUri = null;
    ProgressDialog mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        ButterKnife.bind(this);
        CollapsingToolbarLayout collapsingToolbar =
                findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitleEnabled(true);
        collapsingToolbar.setTitle("Profile");
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        fileuploadProgress = findViewById(R.id.upload_progress);
        firebaseAuth = FirebaseAuth.getInstance();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef = mFirebaseDatabase.getReference();
        profrefname = mFirebaseDatabase.getReference();
        updateRef = mFirebaseDatabase.getReference();
        updateRefprof = mFirebaseDatabase.getReference();
        emptyCv = mFirebaseDatabase.getReference();
        emptyCv.keepSynced(true);
        updateRef.keepSynced(true);
        updateRefprof.keepSynced(true);



        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    finish();
                    Intent moveToHome = new Intent(UserProfileActivity.this, MainActivity.class);
                    moveToHome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(moveToHome);
                }
            }
        };
        String[] user_professions = getResources().getStringArray(R.array.prosuggestion_array);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, user_professions);
        userProfessionEdit.setAdapter(adapter);

        mProgress = new ProgressDialog(this);
        //FIREBASE DATABASE INSTANCE
        myRef = FirebaseDatabase.getInstance().getReference().child("Users").child("UserProfile").child(firebaseAuth.getCurrentUser().getUid());
       proffRefspin = FirebaseDatabase.getInstance().getReference().child("Users").child("Professions").child(firebaseAuth.getCurrentUser().getUid());
        cvRef = FirebaseDatabase.getInstance().getReference().child("Users").child("Resumes").child(firebaseAuth.getCurrentUser().getUid());
        profrefname = FirebaseDatabase.getInstance().getReference().child("Users").child("UserProfile").child(firebaseAuth.getCurrentUser().getUid());
        proffRef = FirebaseDatabase.getInstance().getReference().child("Users").child("Professions").child(firebaseAuth.getCurrentUser().getUid());
        emptyCv = FirebaseDatabase.getInstance().getReference().child("Users").child("ProfessionalResume").child(firebaseAuth.getCurrentUser().getUid());
        mStorageRef = FirebaseStorage.getInstance().getReference();


        profrefname.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
               // String value = userNameEdit.getText().toString();
                //DatabaseReference chilRef = profrefname.child("username");
               // chilRef.setValue(value);


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }

    @OnClick(R.id.userAvatar)
    public void userAvatar(){
        profilePicSelection();
    }

    @OnClick(R.id.createResume)
    public void createResume(){
        createUserResume();
    }

    @OnClick(R.id.createProfile)
    public void createProfile(){
        createUserProfile();
    }
    @OnClick(R.id.updateProf)
    public void updateProf(){
        updateProfile();
    }
    /*
    @OnClick(R.id.saveResume)
    public void saveResume(){
        final Animation myAnim = AnimationUtils.loadAnimation(UserProfileActivity.this, R.anim.bounce);
        Bounce interpolator = new Bounce(0.2, 20);
        myAnim.setInterpolator(interpolator);
        saveResumeBtn.startAnimation(myAnim);
        saveUserResume();
    }*/

    @OnClick(R.id.editSettings)
    public void editSettings(){
        final Animation myAnim = AnimationUtils.loadAnimation(UserProfileActivity.this, R.anim.bounce);
        Bounce interpolator = new Bounce(0.2, 20);
        myAnim.setInterpolator(interpolator);
        editSettingsbtn.startAnimation(myAnim);
        goToSettings();
    }
    @OnClick(R.id.thumb)
    public void thumb(){
        final Animation myAnim = AnimationUtils.loadAnimation(UserProfileActivity.this, R.anim.bounce);
        Bounce interpolator = new Bounce(0.2, 20);
        myAnim.setInterpolator(interpolator);
        fileThumbNail.startAnimation(myAnim);
        Tooltip.make(this,
                new Tooltip.Builder(101)
                        .anchor(fileThumbNail, Tooltip.Gravity.BOTTOM)
                        .closePolicy(new Tooltip.ClosePolicy()
                                .insidePolicy(true, false)
                                .outsidePolicy(true, false), 4000)
                        .activateDelay(900)
                        .showDelay(400)
                        .text("Coming Soon to ECard")
                        .withStyleId(R.style.ToolTipLayoutDefaultStyle_Custom1)
                        .maxWidth(600)
                        .withArrow(true)
                        .floatingAnimation(Tooltip.AnimationBuilder.DEFAULT)
                        .withOverlay(true).build()
        ).show();
    }



    public void createUserResume(){
        startActivity(new Intent(UserProfileActivity.this, CreateRes.class));

    }
    private void goToSettings(){

        startActivity(new Intent(UserProfileActivity.this, ActivitySettings.class));
    }
    private void createUserProfile() {
        String username, userSummary, userStatus, userDetail, userProfession, proffessionid,usrContactEmail;
        username = userNameEdit.getText().toString().trim();
        userSummary = userSummaryEdit.getText().toString().trim();
        userStatus = userStatusEdit.getText().toString().trim();
        //userDetail = userDetailEdit.getText().toString().trim();
        userProfession = userProfessionEdit.getText().toString().trim();
        usrContactEmail = userContactEmailEdit.getText().toString().trim();
        proffessionid = profid.getText().toString().trim();

        //empty cv
        emptyCv.child("skills").setValue("User has not added skills");
        emptyCv.child("address").setValue("User has not added address");
        emptyCv.child("experience").setValue("User has not added experience");
        emptyCv.child("email").setValue("User has not added an email");
        emptyCv.child("website").setValue("User has not added website");
        emptyCv.child("moredetail").setValue("User has not added more details");
        emptyCv.child("phone").setValue("User has not added a phone number");
        // myRef.child("location").setValue(userLocation);
        emptyCv.child("userid").setValue(firebaseAuth.getCurrentUser().getUid());

        myRef.child("username").setValue(username);
        myRef.child("summary").setValue(userSummary);
        myRef.child("status").setValue(userStatus);
        //myRef.child("details").setValue(userDetail);
        myRef.child("profession").setValue(userProfession);
        myRef.child("email").setValue(usrContactEmail);
        myRef.child("userid").setValue(firebaseAuth.getCurrentUser().getUid());

        //profession
        proffRef.child("userid").setValue(firebaseAuth.getCurrentUser().getUid());
        proffRef.child("profession").setValue(userProfession);
        proffRef.child("username").setValue(username);
        proffRef.child("summary").setValue(userSummary);
        proffRef.child("emailcontact").setValue(usrContactEmail);


        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.saved_profile_toast,
                (ViewGroup) findViewById(R.id.custom_toast_container));

        TextView text = layout.findViewById(R.id.text);
        text.setText(R.string.ProfileSaved);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();

        if (imageHoldUri != null) {

            mProgress.setTitle("Update in Progress...");
            mProgress.setMessage("Please wait....");
            mProgress.show();

            StorageReference mChildStorage = mStorageRef.child("User_Profile").child(imageHoldUri.getLastPathSegment());

            String profilePicUrl = imageHoldUri.getLastPathSegment();

            mChildStorage.putFile(imageHoldUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    final Uri imageUrl = taskSnapshot.getDownloadUrl();
                    assert imageUrl != null;
                    myRef.child("imageurl").setValue(imageUrl.toString());
                    mProgress.dismiss();
                    Toast.makeText(UserProfileActivity.this,"Profile Saved Successfully",Toast.LENGTH_LONG).show();

                }
            });
        } else {
            Toast.makeText(UserProfileActivity.this, "Please ensure you have uploaded a profile picture", Toast.LENGTH_LONG).show();
            finish();
            Intent moveToHome = new Intent(UserProfileActivity.this, MainActivity.class);
            moveToHome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(moveToHome);
        }
    }
public void updateProfile(){
        //TODO: Update each child without replacing other values
    updateRefprof = FirebaseDatabase.getInstance().getReference().child("Users").child("Professions").child(firebaseAuth.getCurrentUser().getUid());
    updateRef = FirebaseDatabase.getInstance().getReference().child("Users").child("UserProfile").child(firebaseAuth.getCurrentUser().getUid());
    String username, userSummary, userStatus, userProfession,usrContactEmail;
    username = userNameEdit.getText().toString().trim();
    userSummary = userSummaryEdit.getText().toString().trim();
    userStatus = userStatusEdit.getText().toString().trim();
    //userDetail = userDetailEdit.getText().toString().trim();
    userProfession = userProfessionEdit.getText().toString().trim();
    usrContactEmail = userContactEmailEdit.getText().toString().trim();





    updateRef.child("username").setValue(username);
    updateRef.child("summary").setValue(userSummary);
    updateRef.child("status").setValue(userStatus);
    updateRef.child("profession").setValue(userProfession);
    updateRef.child("email").setValue(usrContactEmail);


    updateRefprof.child("username").setValue(username);
    updateRefprof.child("summary").setValue(userSummary);
    updateRefprof.child("profession").setValue(userProfession);
    updateRefprof.child("email").setValue(usrContactEmail);


}
    public void saveUserResume(){
        if( fileHoldUri != null  )
        {
            mProgress.setTitle("Update in Progress...");
            mProgress.setMessage("Please wait....");
            mProgress.show();
            StorageReference mChildStorage = mStorageRef.child("User_Resumes").child(fileHoldUri.getLastPathSegment());
            mChildStorage.putFile(fileHoldUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    final Uri fileUrl = taskSnapshot.getDownloadUrl();//getDownloadUrl
                    cvRef.child("cvurl").setValue(fileUrl.toString());
                    mProgress.dismiss();
                    Toast.makeText(UserProfileActivity.this,"File Uploaded Successfully",Toast.LENGTH_LONG).show();
                    finish();

                }
            })
                  .addOnFailureListener(new OnFailureListener() {
                      @Override
                      public void onFailure(@NonNull Exception e) {
                          Toast.makeText(UserProfileActivity.this,"File Failed to Upload",Toast.LENGTH_LONG).show();
                      }
                  }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    double progress =(100.0 * taskSnapshot.getBytesTransferred())/taskSnapshot.getTotalByteCount();
                    fileuploadProgress.setProgress((int)progress);

                }
            });
        }
    }

    private void profilePicSelection() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(UserProfileActivity.this);
        builder.setTitle("Add Photo!");
        builder.setIcon(R.drawable.sillyface);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    cameraIntent();
                } else if (items[item].equals("Choose from Library")) {
                    galleryIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
    private void cameraIntent() {
        Log.d("riley", "entered here");
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }
    private void galleryIntent() {
        //CHOOSE IMAGE FROM GALLERY
        Log.d(TAG, "entered here");
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, SELECT_IMAGE);
    }
//upload pdf cv
   /* private void uploadFile(){



        final CharSequence[] items = {"Choose from Library",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(UserProfileActivity.this);
        builder.setTitle("Upload Your Resume!");
        builder.setIcon(R.drawable.upload_icon);

        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Choose from Library")) {
                    fileIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();

    } */

    private void fileIntent() {
        //CHOOSE file FROM GALLERY
        Log.d(TAG, "entered here");
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, SELECT_FILE);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_IMAGE && resultCode == RESULT_OK) {
            Uri imageUri = data.getData();
            CropImage.activity(imageUri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setAspectRatio(1, 1)
                    .start(this);

        }
        if (requestCode == REQUEST_CAMERA && resultCode == RESULT_OK) {
            Uri imageUri = data.getData();
            CropImage.activity(imageUri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setAspectRatio(1, 1)
                    .start(this);
        }
        //image crop library code
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                imageHoldUri = result.getUri();
                userImageavatar.setImageURI(imageHoldUri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
        //select file library code
        if (requestCode == SELECT_FILE && resultCode == RESULT_OK) {
            Uri fileUri = data.getData();
            CropImage.activity(fileUri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setAspectRatio(1, 1)
                    .start(this);

        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {


            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                fileHoldUri = result.getUri();
                fileThumbNail.setImageURI(fileHoldUri);

            }
        }

    }



    }


