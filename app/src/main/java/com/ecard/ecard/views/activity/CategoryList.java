package com.ecard.ecard.views.activity;



import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ecard.ecard.R;
import com.ecard.ecard.views.adapters.CatListAdapter;
import com.ecard.ecard.model.ProfessionsList;
import com.ecard.ecard.views.activity.detailview.ProfileDetail;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import java.util.List;


public class CategoryList extends AppCompatActivity implements CatListAdapter.OnItemClickListener {
    private static final String TAG = "CategoryList";
    public static final String CAT_POSITION = "category-list";
    private List<ProfessionsList> proflistItems;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference myRef;
    private DatabaseReference statusRef;
    private TextView tvName, tvSummary, tvStatus, tvDetail, tvProff;
    private ImageView ivImage;
    private ImageView ivImageCard;
    private Button saveFavorite, btnTrack;
    private CatListAdapter adapter;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recycler_view);
        proflistItems = new ArrayList<>();
        //adapter = new CatListAdapter(proflistItems,this);
        adapter = new CatListAdapter(proflistItems,this);
//        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        getFirebaseData();
        getProfileView();
        //  progressBar = rootView.findViewById(R.id.progressBar);
        recyclerView = findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplication()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
       // int tilePadding = getResources().getDimensionPixelSize(R.dimen.tile_padding);
        //recyclerView.setPadding(tilePadding, tilePadding, tilePadding, tilePadding);
      //  recyclerView.setLayoutManager(new GridLayoutManager(getApplication(), 2));
        recyclerView.setAdapter(adapter);
        //return recyclerView;


    }

    public void getProfileView() {
        ivImage = findViewById(R.id.userAvatar);
        //ivImageCard = findViewById(R.id.header_img);
        tvName = findViewById(R.id.userProfileName);
        tvSummary = findViewById(R.id.uSummary);
        tvStatus = findViewById(R.id.userStatus);
        // tvDetail = findViewById(R.id.profileDetail);
        //tvCardName = findViewById(R.id.carduserName);
        tvProff = findViewById(R.id.proffname);
        btnTrack = findViewById(R.id.starttracking);


    }

    private void getFirebaseData() {
        String userid = getIntent().getExtras().getString(CategoryList.CAT_POSITION);
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef = mFirebaseDatabase.getReference("Users").child("Professions");
        // statusRef = mFirebaseDatabase.getReference("Users").child("UserProfile");
        myRef.child(userid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                ProfessionsList data = dataSnapshot.getValue(ProfessionsList.class);
                proflistItems.add(data);
                adapter.notifyDataSetChanged();
               /* ListItem listItems = dataSnapshot.getValue(ListItem.class);
                if(listItems!=null){
                    tvName.setText(listItems.getUsername());
                   tvSummary.setText(listItems.getSummary());
                    tvStatus.setText(listItems.getStatus());
                    //tvDetail.setText(listItems.getDetails());
                    tvProff.setText(listItems.getProfession());
//                   tvCardName.setText(listItems.getUsername());
                    Glide.with(CategoryList.this)
                            .load(listItems.getImageurl())
                            .into(ivImage);
                }*/
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

       /* FloatingActionButton savefav = findViewById(R.id.savetoFavorite);
        savefav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //open settings
                saveToFavorites();
            }
        });*/

    }




    public void onItemClick(int pos) {

        Intent pDetail = new Intent(this.getApplicationContext(), ProfileDetail.class);

        ProfessionsList listItem = proflistItems.get(pos);
        pDetail.putExtra(ProfileDetail.EXTRA_POSITION, listItem.getUserid());
        startActivity(pDetail);
    }
}






