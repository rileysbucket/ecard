
    package com.ecard.ecard.views.activity.auth;

    import android.app.ProgressDialog;
    import android.content.Intent;
    import android.support.annotation.NonNull;
    import android.support.design.widget.Snackbar;
    import android.os.Bundle;
    import android.text.TextUtils;
    import android.widget.AutoCompleteTextView;
    import android.widget.Button;
    import android.widget.EditText;
    import android.widget.Toast;
    import com.ecard.ecard.views.activity.MainActivity;

    import com.ecard.ecard.data.BaseActivity;
    import com.google.android.gms.tasks.OnCompleteListener;
    import com.google.android.gms.tasks.Task;
    import com.google.firebase.auth.AuthResult;
    import com.google.firebase.auth.FirebaseAuth;
    import com.google.firebase.auth.FirebaseUser;
    import com.google.firebase.database.DataSnapshot;
    import com.google.firebase.database.DatabaseError;
    import com.google.firebase.database.DatabaseReference;
    import com.google.firebase.database.FirebaseDatabase;
    import com.google.firebase.database.ValueEventListener;
    import com.google.firebase.storage.StorageReference;
    import com.ecard.ecard.R;


    import butterknife.BindView;
    import butterknife.ButterKnife;
    import butterknife.OnClick;


    public class RegisterActivity extends BaseActivity {
        private static final String TAG = "RegisterActivity";
        @BindView(R.id.name)
        EditText inputFullName;
        @BindView(R.id.password)
        EditText inputPassword;
        @BindView(R.id.email)
        AutoCompleteTextView inputEmail;


        @BindView(R.id.btnReg) Button btnRegister;
        @BindView(R.id.btnLinkToLoginScreen) Button btnLinkToLogin;
        //@BindView(R.id.btn_reset_password) Button btnResetPassword;

        private FirebaseAuth firebaseAuth;
       private  FirebaseAuth.AuthStateListener mAuthListener;
        private DatabaseReference myRef;
        private FirebaseDatabase mFirebaseDatabase;
        private StorageReference mStorageRef;


        private ProgressDialog pDialog;
        String name,email,password;
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_register);
            ButterKnife.bind(this);
            checkNet();
            assert getSupportActionBar() != null;
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            firebaseAuth = FirebaseAuth.getInstance();

            final FirebaseUser user = firebaseAuth.getCurrentUser();
            pDialog = new ProgressDialog(this);
            pDialog.setCancelable(false);
            pDialog.setTitle("Hi there,Hang on as i register your new ECard Account........");

            if(user != null){
                finish();
                startActivity(new Intent(RegisterActivity.this, MainActivity.class));
            }
        }

        @OnClick(R.id.btnReg)
        public void btnReg(){
            RegisterUser();
        }
        @OnClick(R.id.btnLinkToLoginScreen)
        public void btnLinkToLoginScreen(){
            GoToLogin();
        }

        private  void RegisterUser(){
                    name = inputFullName.getText().toString().trim();
                    email = inputEmail.getText().toString().trim();
                    password = inputPassword.getText().toString().trim();
                    if(TextUtils.isEmpty(name)){

                        Snackbar esnackbar = Snackbar.make(findViewById(R.id.activity_register),
                                R.string.missingname, Snackbar.LENGTH_SHORT);
                        esnackbar.show();
                        return;
                    }
                    if(TextUtils.isEmpty(email)){

                        Snackbar esnackbar = Snackbar.make(findViewById(R.id.activity_register),
                                R.string.missingemail, Snackbar.LENGTH_SHORT);
                        esnackbar.show();
                        return;
                    }
                    if(TextUtils.isEmpty(password)){
                        Snackbar esnackbar = Snackbar.make(findViewById(R.id.activity_register),
                                R.string.missingpassword, Snackbar.LENGTH_SHORT);
                        esnackbar.show();
                        return;
                    }
                    if (password.length() < 6) {
                        Toast.makeText(getApplicationContext(), "Password is too short, enter minimum 6 characters!", Toast.LENGTH_SHORT).show();

                    }
                    showDialog();
                    StartFirebaseRegistration();
                }
        private  void GoToLogin(){
                    Intent i = new Intent(getApplicationContext(),
                            LoginActivity.class);
                    startActivity(i);
                    finish();
                }



        private void StartFirebaseRegistration(){
            firebaseAuth = FirebaseAuth.getInstance();
            mFirebaseDatabase = FirebaseDatabase.getInstance();
            myRef = mFirebaseDatabase.getReference();
            myRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });


            firebaseAuth.createUserWithEmailAndPassword(email,password)
                    .addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(!task.isSuccessful()) {
                                Toast.makeText(RegisterActivity.this, "Authentication failed.", Toast.LENGTH_SHORT).show();
                                hideDialog();
                            }
                            if (task.isSuccessful()) {
                                String userid = firebaseAuth.getCurrentUser().getUid();
                                DatabaseReference current_user_db = FirebaseDatabase.getInstance().getReference("Users").child(userid);
                                current_user_db.setValue(true);
                                Toast.makeText(RegisterActivity.this, "Sign Up Successful" + task.getException(),
                                        Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                                finish();
                                hideDialog();
                            }
                        }
                    });
        }
        private void showDialog() {
            if (!pDialog.isShowing())
                pDialog.show();
        }
        private void hideDialog() {
            if (pDialog.isShowing())
                pDialog.dismiss();
        }

        @Override
        public  void  onStart(){
            super.onStart();


        }
        @Override
        public  void onStop(){

            super.onStop();

        }
    }
