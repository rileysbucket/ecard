package com.ecard.ecard.views.activity.detailview;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.ecard.ecard.R;
import com.ecard.ecard.model.ListItem;
import com.ecard.ecard.views.activity.animations.Bounce;
import com.ecard.ecard.views.activity.likeslibrary.CounterActivity;
import com.ecard.ecard.views.activity.map.MapsActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import it.sephiroth.android.library.tooltip.Tooltip;


public class ProfileDetail extends AppCompatActivity {
    private static final String TAG = "EXTRA_POSITION";
    public static final String EXTRA_POSITION = "profile_detail";
    private static final int ERROR_DIALOG_REQUEST = 9001;


    private List<ListItem> listItems;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference myRef, resumeRef, mDatabaseLike,proffRef, emailRef,calRef;
    private String tBarUsername, tBarUser;
    private ProgressDialog progressDialog;
    private boolean mProcessLike = false;
    private Context mContext;
    private Activity mActivity;

    private FrameLayout mRelativeLayout;
    private Button mButton;

    private PopupWindow mPopupWindow;

    private RadioGroup mCounterMode;



    //@BindView(R.id.userPName) TextView uname;
    @BindView(R.id.userProfileName)
    TextView tvName;
    @BindView(R.id.userStatus)
    TextView tvStatus;
    @BindView(R.id.uSummary)
    TextView tvSummary;
    //@Bind(R.id.profileDetail) TextView tvDetail;
    @BindView(R.id.proffname)
    TextView tvProff;
    @BindView(R.id.emailContact)
    TextView tvEmail;


    //imageviews
    @BindView(R.id.userAvatar)
    ImageView ivImage;
    @BindView(R.id.header_img)
    ImageView ivImageCard;
    //buttons
    @BindView(R.id.starttracking)
    FloatingActionButton btnTrack;
    @BindView(R.id.userportfolio)
    FloatingActionButton userPortfolio;
    @BindView(R.id.chat)
    FloatingActionButton chatFab;
    @BindView(R.id.hire)
    FloatingActionButton hirePerson;
    @BindView(R.id.savetoFavorite)
    CounterActivity mCounterFab;
    @BindView(R.id.share)
    FloatingActionButton shareProf;
    @BindView(R.id.bookmark)
    FloatingActionButton bookmarkProf;
    @BindView(R.id.jobs)
    FloatingActionButton Jobs;
    @BindView(R.id.viewresume)
    FloatingActionButton viewResume;


    //dialog views
    //TODO:Bind textviews for dialog to butterknife
    // @BindView(R.id.userPName) TextView uname;

    String usremail;
    String upro;
    String userNum;
    String uemail;


    String each_key = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_detailview);
        ButterKnife.bind(this);
        final CollapsingToolbarLayout collapsingToolbar =
                findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitleEnabled(true);
        //collapsingToolbar.setTitle("Profile");
        setSupportActionBar((Toolbar) findViewById(R.id.proftoolbar));
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        listItems = new ArrayList<>();
        progressDialog = new ProgressDialog(this);
        String userId = Objects.requireNonNull(getIntent().getExtras()).getString(ProfileDetail.EXTRA_POSITION);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        assert user != null;
        String uid = user.getUid();
        //Database References
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef = mFirebaseDatabase.getReference("Users").child("UserProfile");
        resumeRef = mFirebaseDatabase.getReference("Users").child("ProfessionalResume");
        mDatabaseLike = mFirebaseDatabase.getReference("Users").child("Likes");
        proffRef = FirebaseDatabase.getInstance().getReference().child("Users").child("Professions").child(uid);

        emailRef = mFirebaseDatabase.getReference("Users").child("UserProfile").child(uid);
        calRef = mFirebaseDatabase.getReference("Users").child("ProfessionalResume").child(uid);
        //emailRef = FirebaseDatabase.getInstance().getReference().child("Users").child("UserProfile").child(uid);
        //keep synced
        myRef.keepSynced(true);
        mDatabaseLike.keepSynced(true);
        resumeRef.keepSynced(true);

        //progress dialog
        progressDialog.show();
        //mRef
        assert userId != null;
        myRef.child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.exists()) {
                    ListItem listItems = dataSnapshot.getValue(ListItem.class);
                    if (listItems != null) {
                        tvName.setText(listItems.getUsername());
                        tvSummary.setText(listItems.getSummary());
                        tvStatus.setText(listItems.getStatus());
                        tvProff.setText(listItems.getProfession());
                        tvEmail.setText(listItems.getEmail());
                        Glide.with(ProfileDetail.this)
                                .load(listItems.getImageurl())
                                .into(ivImage);
                        tBarUsername = (String) dataSnapshot.child("username").getValue();
                        collapsingToolbar.setTitle(tBarUsername);

                        tBarUser = (String) dataSnapshot.child("username").getValue();
                        collapsingToolbar.setTitle(tBarUsername);
                        uemail = (String) dataSnapshot.child("email").getValue();
                    }
                    progressDialog.dismiss();
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(ProfileDetail.this, "Data failed to load,Check your Internet and Try again", Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressDialog.dismiss();
            }
        });
    }

    //button functions
    @OnClick(R.id.starttracking)
    public void starttracking() {
        final Animation myAnim = AnimationUtils.loadAnimation(ProfileDetail.this, R.anim.bounce);
        Bounce interpolator = new Bounce(0.2, 20);
        myAnim.setInterpolator(interpolator);
        btnTrack.startAnimation(myAnim);
        trackUser();
    }

    @OnClick(R.id.chat)
    public void chat() {
        final Animation myAnim = AnimationUtils.loadAnimation(ProfileDetail.this, R.anim.bounce);
        Bounce interpolator = new Bounce(0.2, 20);
        myAnim.setInterpolator(interpolator);
        chatFab.startAnimation(myAnim);
        startChat();
    }

    @OnClick(R.id.hire)
    public void hire() {
        final Animation myAnim = AnimationUtils.loadAnimation(ProfileDetail.this, R.anim.bounce);
        Bounce interpolator = new Bounce(0.2, 20);
        myAnim.setInterpolator(interpolator);
        hirePerson.startAnimation(myAnim);

        Tooltip.make(this,
                new Tooltip.Builder(101)
                        .anchor(hirePerson, Tooltip.Gravity.BOTTOM)
                        .closePolicy(new Tooltip.ClosePolicy()
                                .insidePolicy(true, false)
                                .outsidePolicy(true, false), 4000)
                        .activateDelay(900)
                        .showDelay(400)
                        .text("Hire me Coming Soon to ECard")
                        .withStyleId(R.style.ToolTipLayoutDefaultStyle_Custom1)
                        .maxWidth(600)
                        .withArrow(true)
                        .floatingAnimation(Tooltip.AnimationBuilder.DEFAULT)
                        .withOverlay(true).build()
        ).show();
        //hireFunction();
    }

    @OnClick(R.id.share)
    public void share() {
        final Animation myAnim = AnimationUtils.loadAnimation(ProfileDetail.this, R.anim.bounce);
        Bounce interpolator = new Bounce(0.2, 20);
        myAnim.setInterpolator(interpolator);
        shareProf.startAnimation(myAnim);
        shareProfile();
    }

    @OnClick(R.id.savetoFavorite)
    public void savetoFavorite() {
        final Animation myAnim = AnimationUtils.loadAnimation(ProfileDetail.this, R.anim.bounce);
        Bounce interpolator = new Bounce(0.2, 20);
        myAnim.setInterpolator(interpolator);
        mCounterFab.startAnimation(myAnim);
        saveToFavorites();
    }

    @OnClick(R.id.bookmark)
    public void bookmark() {
        final Animation myAnim = AnimationUtils.loadAnimation(ProfileDetail.this, R.anim.bounce);
        Bounce interpolator = new Bounce(0.2, 20);
        myAnim.setInterpolator(interpolator);
        bookmarkProf.startAnimation(myAnim);
        bookmarkProfile();
    }

    @OnClick(R.id.userportfolio)
    public void userportfolio() {
        final Animation myAnim = AnimationUtils.loadAnimation(ProfileDetail.this, R.anim.bounce);
        Bounce interpolator = new Bounce(0.2, 20);
        myAnim.setInterpolator(interpolator);
        userPortfolio.startAnimation(myAnim);
        Tooltip.make(this,
                new Tooltip.Builder(101)
                        .anchor(userPortfolio, Tooltip.Gravity.BOTTOM)
                        .closePolicy(new Tooltip.ClosePolicy()
                                .insidePolicy(true, false)
                                .outsidePolicy(true, false), 4000)
                        .activateDelay(900)
                        .showDelay(400)
                        .text("Portfolio Coming Soon to ECard")
                        .withStyleId(R.style.ToolTipLayoutDefaultStyle_Custom1)
                        .maxWidth(600)
                        .withArrow(true)
                        .floatingAnimation(Tooltip.AnimationBuilder.DEFAULT)
                        .withOverlay(true).build()
        ).show();
        completedJobs();
    }

    @OnClick(R.id.jobs)
    public void jobs() {
        final Animation myAnim = AnimationUtils.loadAnimation(ProfileDetail.this, R.anim.bounce);
        Bounce interpolator = new Bounce(0.2, 20);
        myAnim.setInterpolator(interpolator);
        Jobs.startAnimation(myAnim);
        Tooltip.make(this,
                new Tooltip.Builder(101)
                        .anchor(Jobs, Tooltip.Gravity.BOTTOM)
                        .closePolicy(new Tooltip.ClosePolicy()
                                .insidePolicy(true, false)
                                .outsidePolicy(true, false), 4000)
                        .activateDelay(900)
                        .showDelay(400)
                        .text("Coming Soon to ECard")
                        .withStyleId(R.style.ToolTipLayoutDefaultStyle_Custom1)
                        .maxWidth(600)
                        .withArrow(true)
                        .floatingAnimation(Tooltip.AnimationBuilder.DEFAULT)
                        .withOverlay(true).build()
        ).show();
        completedJobs();
    }

    @OnClick(R.id.viewresume)
    public void viewresume() {
        final Animation myAnim = AnimationUtils.loadAnimation(ProfileDetail.this, R.anim.bounce);
        Bounce interpolator = new Bounce(0.2, 20);
        myAnim.setInterpolator(interpolator);
        viewResume.startAnimation(myAnim);
        ViewResume();
    }

    //Methods
    //ViewResume
    private void ViewResume() {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.activity_viewresume, null);


        final FloatingActionButton emailFab = alertLayout.findViewById(R.id.email_fab);
        final FloatingActionButton callFab = alertLayout.findViewById(R.id.call_fab);

        emailFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Animation myAnim = AnimationUtils.loadAnimation(ProfileDetail.this, R.anim.bounce);
                Bounce interpolator = new Bounce(0.2, 20);
                myAnim.setInterpolator(interpolator);
                emailFab.startAnimation(myAnim);
                emailUserMethod();
            }
        });

        callFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Animation myAnim = AnimationUtils.loadAnimation(ProfileDetail.this, R.anim.bounce);
                Bounce interpolator = new Bounce(0.2, 20);
                myAnim.setInterpolator(interpolator);
                callFab.startAnimation(myAnim);
                callMethod();
            }
        });

        // uname = alertLayout.findViewById(R.id.userPName);
        final TextView uname = alertLayout.findViewById(R.id.userPName);
        final TextView tvfullName = alertLayout.findViewById(R.id.user_fullname);
        final TextView tvuserSkills = alertLayout.findViewById(R.id.user_skills);
        final TextView tvExperience = alertLayout.findViewById(R.id.user_experience);
        final TextView tvuserWebsite = alertLayout.findViewById(R.id.user_website);
        final TextView tvcontactEmail = alertLayout.findViewById(R.id.contact_email);
        final TextView tvuserMore = alertLayout.findViewById(R.id.moreaboutyou);
        final TextView tvPhone = alertLayout.findViewById(R.id.user_phone_number);
        final TextView tvAddress = alertLayout.findViewById(R.id.postal_address);
        final TextView tvProfession = alertLayout.findViewById(R.id.userprofession);


        String userId = getIntent().getExtras().getString(ProfileDetail.EXTRA_POSITION);
        assert userId != null;
        myRef.child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (listItems != null) {
                    ListItem listItems = dataSnapshot.getValue(ListItem.class);

                    assert listItems != null;
                    uname.setText(listItems.getUsername());
                    tvfullName.setText(listItems.getUsername());


                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        proffRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot uprofession) {

                upro = (String) uprofession.child("profession").getValue();
                tvProfession.setText(upro);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("error", databaseError.getMessage());
            }
        });
        resumeRef.child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (listItems != null) {
                    ListItem listItems = dataSnapshot.getValue(ListItem.class);
                    assert listItems != null;

                    tvuserSkills.setText(listItems.getSkills());
                    tvExperience.setText(listItems.getExperience());
                    tvuserWebsite.setText(listItems.getWebsite());
                    tvcontactEmail.setText(listItems.getEmail());
                    tvuserMore.setText(listItems.getMoredetail());
                    tvAddress.setText(listItems.getAddress());
                    tvPhone.setText(listItems.getPhone());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });


        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Resume");
        alert.setIcon(R.drawable.cv_folder_blue_24dp);
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);

        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(false);
        alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getBaseContext(), "Closed", Toast.LENGTH_SHORT).show();
            }
        });

        AlertDialog dialog = alert.create();
        dialog.show();
    }

    public void emailUserMethod() {

        emailRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot emailusr) {

                usremail = (String) emailusr.child("username").getValue();
                //uname.setText(usrname);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("error", databaseError.getMessage());
            }
        });

        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{uemail});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "ECard Contact");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Hello");
        startActivity(Intent.createChooser(emailIntent, "Send email via..."));
    }


    public void callMethod() {

        calRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot calluser) {

                userNum = (String) calluser.child("phone").getValue();
                //uname.setText(usrname);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("error", databaseError.getMessage());
            }
        });

        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + userNum));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }


    private void completedJobs() {
    }
    private void trackUser() {
        Intent intent = new Intent(ProfileDetail.this, MapsActivity.class);
        startActivity(intent);

        }
   private void startChat(){
       Tooltip.make(this,
               new Tooltip.Builder(101)
                       .anchor(chatFab, Tooltip.Gravity.BOTTOM)
                       .closePolicy(new Tooltip.ClosePolicy()
                               .insidePolicy(true, false)
                               .outsidePolicy(true, false), 4000)
                       .activateDelay(900)
                       .showDelay(400)
                       .text("Coming Soon to ECard")
                       .withStyleId(R.style.ToolTipLayoutDefaultStyle_Custom1)
                       .maxWidth(600)
                       .withArrow(true)
                       .floatingAnimation(Tooltip.AnimationBuilder.DEFAULT)
                       .withOverlay(true).build()
       ).show();

       // Intent c = new Intent(ProfileDetail.this, ChatActivity.class);
        //startActivity(c);
    }
    private void hireFunction() {
        AlertDialog.Builder hireAlert = new AlertDialog.Builder(this);
        hireAlert.setIcon(R.drawable.ic_hire_blue_24dp);
        hireAlert.setMessage("Proceed to Hire this talent");
        hireAlert.setTitle("Hire");
        hireAlert.setPositiveButton("Continue",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        Toast.makeText(ProfileDetail.this,"You clicked yes button",Toast.LENGTH_LONG).show();
                        Intent b = new Intent(ProfileDetail.this,Hire.class);
                        startActivity(b);
                    }
                });
        hireAlert.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        AlertDialog alertDialog = hireAlert.create();
        alertDialog.show();
    }

    private void shareProfile() {
        Intent sendIntent = new Intent();
       // String msg = "Hey, check this out: " + myDynamicLink;
        sendIntent.setAction(Intent.ACTION_SEND);
        //sendIntent.putExtra(Intent.EXTRA_TEXT, msg);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);

    }
    private void bookmarkProfile() {
    }
    private void saveToFavorites() {
        mCounterFab.increase();

        mProcessLike = true;
        if (mProcessLike) {
            mDatabaseLike.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }


//OnStart,OnStop,OnResume
    public void onStart(){
        super.onStart();


    }
    public void onStop(){
        super.onStop();


    }
    public void onResume(){
        super.onResume();


    }


}






