package com.ecard.ecard.views.activity.chat;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.ecard.ecard.R;
import com.ecard.ecard.views.activity.animations.Bounce;

public class ChatActivity extends AppCompatActivity {
    private static final String TAG = "ChatActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        final FloatingActionButton chatFab = findViewById(R.id.send);
        chatFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Animation myAnim = AnimationUtils.loadAnimation(ChatActivity.this, R.anim.bounce);
                Bounce interpolator = new Bounce(0.2, 20);
                myAnim.setInterpolator(interpolator);
                chatFab.startAnimation(myAnim);

            }
        });
    }

}
