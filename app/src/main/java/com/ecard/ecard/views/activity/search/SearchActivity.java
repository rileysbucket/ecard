package com.ecard.ecard.views.activity.search;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.ecard.ecard.R;
import com.ecard.ecard.model.Users;
import com.ecard.ecard.views.activity.animations.Bounce;
import com.ecard.ecard.views.activity.detailview.ProfileDetail;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class SearchActivity  extends AppCompatActivity {
    private static final String TAG = "Search";
    private DatabaseReference mUserDatabase;
    private FirebaseDatabase mFirebaseDatabase;
    public String searchText;
    private ProgressBar progressBar;

    @BindView(R.id.result_list) RecyclerView mResultList;
    @BindView(R.id.search_field) SearchView mSearchField;
    @BindView(R.id.search_btn)
    FloatingActionButton mSearchBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);


        ButterKnife.bind(this);
        progressBar = findViewById(R.id.search_progress);
        mUserDatabase = FirebaseDatabase.getInstance().getReference("Users").child("UserProfile");
        mSearchField.setSubmitButtonEnabled(true);
        mResultList.setHasFixedSize(true);
        mResultList.setLayoutManager(new LinearLayoutManager(getApplication()));
        mResultList.setItemAnimator(new DefaultItemAnimator());

    }

    @OnClick(R.id.search_btn)
    public void search_btn(){
        performSearch();

    }
    private void firebaseUserSearch(String searchText) {
        mUserDatabase.keepSynced(true);
        progressBar.setVisibility(View.VISIBLE);
        Toast.makeText(SearchActivity.this, "Searching......", Toast.LENGTH_LONG).show();

        final Query SearchQuery = mUserDatabase.orderByChild("profession").startAt(searchText).endAt(searchText + "\uf8ff");
        FirebaseRecyclerAdapter<Users, UsersViewHolder> mAdapter = new FirebaseRecyclerAdapter<Users, UsersViewHolder>(

                Users.class,
                R.layout.search_list_layout,
                UsersViewHolder.class,
                SearchQuery
        ) {
            @Override
            protected void populateViewHolder(UsersViewHolder viewHolder, Users model, final int position) {

                viewHolder.setDetails(getApplicationContext(), model.getUsername(), model.getSummary(), model.getImageurl());
                final String single_view = getRef(position).getKey();

                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent pDetail = new Intent(SearchActivity.this, ProfileDetail.class);
                        pDetail.putExtra(ProfileDetail.EXTRA_POSITION,single_view);
                        startActivity(pDetail);
                    }
                });
            }
        };
        mResultList.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
        progressBar.setVisibility(View.GONE);
    }
    public static class UsersViewHolder extends RecyclerView.ViewHolder implements OnItemClickListener  {
        View mView;
        private TextView user_name;
        private TextView user_summary;
        private CircleImageView user_image;
        public UsersViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            user_image = mView.findViewById(R.id.profile_image);
            user_name = mView.findViewById(R.id.name_text);
            user_summary = mView.findViewById(R.id.summary_text);
        }
        public void setDetails(Context ctx, String userName,String userSummary,String userImage){

            user_name.setText(userName);
            user_summary.setText(userSummary);
            Glide.with(ctx).load(userImage).into(user_image);
        }
        @Override
        public void onItemClick(int pos) {
        }
    }
    public interface OnItemClickListener {
        void onItemClick(int pos);
    }
public  void hideKeyboard(){
    View view = this.getCurrentFocus();
    if (view != null) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
public void performSearch(){

    final Animation myAnim = AnimationUtils.loadAnimation(SearchActivity.this, R.anim.bounce);
    Bounce interpolator = new Bounce(0.1, 10);
    myAnim.setInterpolator(interpolator);
    mSearchBtn.startAnimation(myAnim);
    String searchText = mSearchField.getQuery().toString();
    firebaseUserSearch(searchText);
    hideKeyboard();
}
}






