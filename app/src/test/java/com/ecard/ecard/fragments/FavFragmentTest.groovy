package com.ecard.ecard.fragments

import com.ecard.ecard.views.fragments.FavFragment

//import groovy.transform.CompileStatic
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

//@CompileStatic
@RunWith(RobolectricTestRunner.class)
class FavFragmentTest {

    private FavFragment favFragmentUnderTest

    @Before
    void setUp() {
        favFragmentUnderTest = new FavFragment()
    }

    @Test
    void testOnItemClick() {
        // Setup
        def pos = 0

        // Run the test
        favFragmentUnderTest.onItemClick(pos)

        // Verify the results
    }
}
