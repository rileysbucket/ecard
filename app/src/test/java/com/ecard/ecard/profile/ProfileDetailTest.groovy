package com.ecard.ecard.profile

import android.content.Intent
import com.ecard.ecard.views.activity.detailview.ProfileDetail
//import groovy.transform.CompileStatic
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.android.controller.ActivityController

//@CompileStatic
@RunWith(RobolectricTestRunner.class)
class ProfileDetailTest {

    @Test
    void testLaunchActivity() {
        // Setup
        final Intent intent = new Intent()
        final ActivityController<ProfileDetail> activityController = Robolectric.buildActivity(ProfileDetail.class, intent)

        // Run the test
        final ProfileDetail profileDetailUnderTest = activityController.setup().get()

        // Verify the results
    }
}
